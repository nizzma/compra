habitual = ["patatas", "leche", "pan"]

def obtener_compra_especifica():
    especifica = []
    while True:
        item = input("Elemento a comprar: ")
        if item == "":
            break
        especifica.append(item)
    return especifica

def main():
    especifica = obtener_compra_especifica()

    compra_total = list(set(habitual + especifica))

    print("Lista de la compra:")
    for item in compra_total:
        print(item)

    elementos_habituales = len(habitual)
    elementos_especificos = len(especifica)
    elementos_totales = len(compra_total)

    print(f"Elementos habituales: {elementos_habituales}")
    print(f"Elementos específicos: {elementos_especificos}")
    print(f"Elementos en lista: {elementos_totales}")

if __name__ == "__main__":
    main()